require_relative 'lib/dispatcher.rb'
require_relative 'lib/build.rb'
require 'open-uri'
require 'json'

  DEPO_LOCATION = {
    'latitude' => -37.816664,
    'longitude' => 144.963848
  }
  
  drones_data = open('https://codetest.kube.getswift.co/drones').read
  drones = Build.drones(JSON.parse(drones_data), DEPO_LOCATION)
  
  packages_data = open('https://codetest.kube.getswift.co/packages').read
  packages = Build.packages(JSON.parse(packages_data), DEPO_LOCATION)

  dispatcher = Dispatcher.new(drones, packages, DEPO_LOCATION)
  puts dispatcher.output # output the solution to the terminal