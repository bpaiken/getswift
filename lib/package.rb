require_relative 'haversine.rb'

class Package
  attr_reader(
    :destination, :id, :deadline, 
    :depo_location, :leave_by_time
    )

  def initialize(package_data, depo_location, carrier_speed=50)
    @destination = package_data['destination']
    @id = package_data['packageId']
    @deadline = package_data['deadline']
    @depo_location = depo_location
    @carrier_speed = carrier_speed
    @leave_by_time = reset_leave_by_time
  end
  
  def reset_leave_by_time
    time_to_deliver = distance * 3600 / @carrier_speed
    deadline - time_to_deliver
  end

  def priority
    leave_by_time
  end
  
  private
  def distance
    Haversine.distance(depo_location, destination)
  end
end