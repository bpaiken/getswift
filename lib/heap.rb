# A binary heap implementation that accepts a custom comparator
class Heap
  attr_reader :data

  def initialize(comparator=nil)
    @data = []
    @comparator = comparator || proc { |a, b| a < b }
  end

  def empty?
    @data.empty?
  end

  def size
    @data.size
  end

  def peek
    @data.first
  end

  def compare(a, b)
    return @comparator.call(a, b)
  end

  def insert(item)
    @data << item
    pos = @data.length - 1
    while pos > 0
      parent = ((pos + 1) / 2).floor - 1

      if compare(@data[parent], @data[pos])
        @data[pos], @data[parent] = @data[parent], @data[pos]
        pos = parent
      else
        return
      end
    end
  end

  def remove
    return nil if empty?
    return @data.pop if size == 1

    retval, @data[0] = @data[0], @data.pop

    pos = 0
    loop do
      left = ((pos + 1) * 2) - 1
      right = left + 1
      next_child = left

      break if left >= size

      if right < size
        next_child = right unless compare(@data[right], @data[left])
      end

      break if compare(@data[next_child], @data[pos])

      @data[next_child], @data[pos] = @data[pos], @data[next_child]
      pos = next_child
    end

    return retval
  end

  alias :pop :remove
  alias :push :insert
end