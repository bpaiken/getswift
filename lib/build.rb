require_relative 'drone.rb'
require_relative 'package.rb'

# Build class contains to helper methods that take in raw JSON data returns a collection of drone or package objects
class Build
  # initial drones have already been dispatched...so builder must set depo location
  def self.drones(drones, depo_location)
    drones.map{ |drone| Drone.new(drone, depo_location)}
  end

  def self.packages(packages, depo_location)
    packages.map{ |package| Package.new(package, depo_location)}
  end
end