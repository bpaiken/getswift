require_relative 'heap.rb'

class PackageQueue
  def initialize(packages)
    @depo_location
    @store = Heap.new(proc { |a,b| a.priority > b.priority })
    fill_store(packages)
  end

  def empty?
    @store.empty?
  end

  def remove
    return 'no more packages' if @store.empty?
    @store.remove
  end

  # # insert is not currently used - future implementation
  # def insert(package)
  #   @store.insert(package)
  # end

  private
  def fill_store(packages)
    packages.each do |package|
      @store.insert(package)
    end
  end
end

