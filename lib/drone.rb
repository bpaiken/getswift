require_relative 'package.rb'
require_relative 'haversine.rb'

class Drone
  attr_accessor :depo_location # depo location set by dispatcher via queue
  attr_reader(
    :package, :location, :id, 
    :return_time, :speed
  )

  def initialize(drone_data, depo_location, speed=50)
    @speed = speed
    @depo_location = depo_location
    @id = drone_data['droneId']
    @location = drone_data['location']
    package_data = drone_data['packages'][0]
    @package = set_initial_package(package_data)
    @return_time = reset_return_time
  end

  # # receive_package is not currently used - future implementation
  # def receive_package(package)
  #   raise 'already carrying package' unless @package.nil?
  #   @package = package
  # end

  # # remove package is not currently used - future implementatoin
  # def remove_package
  #   @package = nil
  # end

  def reset_return_time
    @return_time = Time.now.to_i + remaining_trip_time
  end
  
  private
  def destination
    package ? package.destination : depo_location
  end

  def package_distance
    (Haversine.distance(location, package.destination) + 
      Haversine.distance(package.destination, depo_location))
  end

  def no_package_distance
    Haversine.distance(location, depo_location)
  end

  def remaining_trip_time
    return 0 if depo_location == location
    if package
      remaining_distance = package_distance
      remaining_time = (remaining_distance / 50) * 3600
    else
      remaining_time =  no_package_distance * 3600 / 50
    end
  end

  # build package object from the get /drones api response
  def set_initial_package(package_data)
    package_data ? Package.new(package_data, depo_location, speed) : nil
  end
end