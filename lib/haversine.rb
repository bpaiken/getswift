# The haversine formula determines the great-circle distance between two points on a sphere given their longitudes and latitudes.

class Haversine
  EARTH_RADIUS = 6371 # kilometers
  RADIAN_PER_DEGREE = Math::PI / 180.0

  def self.distance(location1, location2)
    lat1, lat2 = location1['latitude'], location2['latitude']
    lng1, lng2 = location1['longitude'], location2['longitude']

    lat1_radians = location1['latitude'] * RADIAN_PER_DEGREE
    lat2_radians = location2['latitude'] * RADIAN_PER_DEGREE

    distance_lat = (lat2-lat1) * RADIAN_PER_DEGREE
    distance_lng = (lng2-lng1) * RADIAN_PER_DEGREE

    a = Math.sin(distance_lat/2)**2 + Math.cos(lat1_radians) * Math.cos(lat2_radians) * Math.sin(distance_lng/2) ** 2
    c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a))
    EARTH_RADIUS * c
  end
end