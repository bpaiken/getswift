require_relative 'package_queue.rb'
require_relative 'drone_queue.rb'

class Dispatcher
  attr_reader :assignments, :unassigned_package_ids, :depo_location

  def initialize(drones, packages, depo_location)
    @package_queue = PackageQueue.new(packages)
    @drone_queue = DroneQueue.new(drones)
    @assignments = []
    @unassigned_package_ids = []

    assign_packages
  end

  def output
    {
      assignments: @assignments,
      unassignedPackageIds: @unassigned_package_ids
    }
  end

  private
  def assign_packages
    until @package_queue.empty? || @drone_queue.empty?
      package = @package_queue.remove
      next_drone = @drone_queue.peek

      if valid_assignment?(next_drone, package)
        drone = @drone_queue.remove
        @assignments << {droneId: drone.id, packageId: package.id}
      else
        @unassigned_package_ids << package.id
      end
    end
  end

  def valid_assignment?(drone, package)
    drone.return_time < package.leave_by_time
  end

end