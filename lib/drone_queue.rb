require_relative 'heap.rb'

class DroneQueue
  def initialize(drones)
    @store = Heap.new( proc { |a,b| a.return_time > b.return_time })

    fill_store(drones)
  end

  def empty?
    @store.empty?
  end

  def peek
    @store.peek
  end

  def remove
    @store.remove
  end

  # # insert is not currently used - future implementation
  # def insert(drone)
  #   @store.insert(drone)
  # end

  private
  def fill_store(drones)
    drones.each do |drone|
      @store.insert(drone)
    end
  end
end