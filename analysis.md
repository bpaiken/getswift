# GetSwift Code Test - Analysis

## Setup
You must have Ruby installed to run the solution.  From a terminal window, navigate to the root folder and run ```ruby getswift.rb```.

## Approach

### Summary

I took a very object oriented approach to solving this problem.  I chose to do it this way because it makes the code more approachable and easier to follow.  Furthermore, it allows anyone to easily add enhancements in the future without having to worry about breaking what has already been written (separation of concerns).  The solution uses a heap data structure to implement drone and package priority queues. These queues allow a dispatcher object to assign packages to drones. The solution has a time complexity of O(n*log(n))

### Comparison

Another approach I considered was to simply sort the API responses based on a drone return time and a package leave by time.  This approach also has a time complexity of O(n*log(n)), but will always execute faster than the object oriented approach for a couple of reasons: 

- 1)  There is a lot of overhead involved with creating object instances for every drone and package.  

- 2)  Although both approaches have a time complexity that reduces to O(n*log(n)), the time complexity of the object oriented approach is actually higher than that of a simple sort solution.  This is because when removing an object from the priority queue, the queue must perform a 'heapify' operation.  This results in the removal of an object from the queue having a time complexity of log(n).

Despite the facts above, I choose to implement the object oriented approach, along with priority queues, for several reasons:

- 1) By separating the concerns of each class, the code is easier to follow and thus more maintainable.  I would imagine that in an actual implementation, many calculations would take place with respect to drones, packages, dispatchers, etc.  Giving them their own classes allows those calculations to be nicely contained within the objects that they pertain too.  

- 2) Although slightly more expensive than a simple sort, the priority queue allows for changes to the dispatch system.  For example, let's say that that a surge of package deliveries is causing an unreasonable number of undeliverable packages.  A collection of drones (maybe assigned to another depo), could easily be reassigned and added into the priority queue without the need to re-sort every drone by its 'return-time'.  This would work the same way for package deliveries as well.  Packages could be added into the queue as their orders are placed, allowing for a dynamic solution to the dispatcher problem.  I believe this capability is much more valuable in the real world than achieving a slightly lower time complexity for a FIXED number of drones/packages.

- 3) It allows me to show you that I understand object oriented programming, as well as the trade-offs it brings. 

## Structure
The lib folder contains the classes used in the solution:

#### ```Drone```
- A drone object is constructed using drone data from the API.  On initialization, the object will calculate and save its ```return_time``` based on it's current location, package status, and depo location. ```return_time``` is the time the drone is expected to arrive at the depo.

#### ```Package```
- A package object is constructed using package data from the API.  On initialization, the object will calculate and save its ```leave_by_time``` based on it's destination, deadline, and depo location. ```leave_by_time``` is the time the package must leave by in order to meet its deadline.

#### ```DroneQueue```
- The drone queue uses a heap data structure to implement a priority queue of drones.  The 'priority' of each drone is based on its ```return_time``` (the time it will return to the depo).

#### ```PackageQueue```
- The package queue uses a heap data structure to implement a priority queue of packages.  The 'priority' of each package is based on its ```leave_by_time```(the time the package must leave by to meet its deadline).

#### ```Dispatcher```
- This dispatcher is used to assign packages to drones.  It does this with the help of package and drone priority queues, which it constructs using collections of package and drone objects.

#### ```Build```
- The build class takes in the json responses from the API, and returns a collection of package or drone objects.

#### ```Heap```
- Instances of the heap class are used by both the drone queue and the package queue to implement priority queues.  Although not require for this problem, a priority queue is an efficient solution to allow new drones or new packages to enter their respective queues.

#### ```Haversine```
- The Haversine formula is used to calculate distance between two points on a sphere given their longitudes and latitudes.  
